import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class iekei_order_machine {
    private JPanel root;
    private JButton ramenButton;
    private JButton ramenMAXButton;
    private JButton bigRiceButton;
    private JButton uzuraMashiButton;
    private JButton hourensouMashiButton;
    private JButton beerButton;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JLabel topLabel;
    private JLabel secondLabel;
    private JTextPane textPane2;
    int total = 0;

    void order(String food,int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(food == "Ramen" ||food =="RamenMAX"){
            JOptionPane.showMessageDialog(null, "Please tell the staff how strong the taste is");
        }
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering  " + food  +"! It will be as soon as possible.");
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food +" "+ price +"yen"+"\n");
            total += price;
            textPane2.setText("Total "+ total +"yen");
        }
    }



    public iekei_order_machine() {
        ramenButton.addActionListener(new ActionListener() {
            @Override
 public void actionPerformed(ActionEvent e) {
order("Ramen",750);
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("mujyaki ramen.jpg")
        ));
        ramenMAXButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("RamenMAX",950);
            }
        });
        ramenMAXButton.setIcon(new ImageIcon(
                this.getClass().getResource("mujyaki tokusei ramen.jpg")
        ));

        bigRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Big Rice",200);
            }
        });
        bigRiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("iekei yamamori rice.jpg")
        ));


        uzuraMashiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Uzura",100);
            }
        });
        uzuraMashiButton.setIcon(new ImageIcon(
                this.getClass().getResource("uzura.jpg")
        ));


        hourensouMashiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hourensou",50);
            }
        });
        hourensouMashiButton.setIcon(new ImageIcon(
                this.getClass().getResource("hourennsou.jpg")
        ));


        beerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("beer",500);
            }
        });
        beerButton.setIcon(new ImageIcon(
                this.getClass().getResource("biru.jpg")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ total +" yen.");
                total=0;
                textPane1.setText("");
                textPane2.setText("");

            }
        });
    }

    public static void main (String[]args){
        JFrame frame = new JFrame("iekei_order_machine");
        frame.setContentPane(new iekei_order_machine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}


